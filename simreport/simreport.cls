% ---------------------------------------------------------------------
% Conference proceedings and article templates for
% personal open-archiving activities
% September 2012
% ---------------------------------------------------------------------

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{simreport}[25/01/2012, v1.0]
\RequirePackage{ifthen}
\RequirePackage{calc}
\AtEndOfClass{\RequirePackage{microtype}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions*
\LoadClass{article}

\RequirePackage{times}           % Loads the Times-Roman Fonts
\RequirePackage{mathptmx}        % Loads the Times-Roman Math Fonts
\RequirePackage{ifpdf}           % Needed to pick between latex and pdflatex
\RequirePackage{type1cm}         % Stefan: scalable Computer Modern fonts


% ---------------------------------------------------------------------
\RequirePackage[utf8]{inputenc}
\RequirePackage{amsmath,amsfonts,amssymb}
\RequirePackage{graphicx,xcolor}
%\UsePackage[english]{babel} % Ovo je isključeno da se može koristiti Hrvatski babel
\RequirePackage{booktabs}
% ---------------------------------------------------------------------

%%%%% for abstract+authors frames 


% ---------------------------------------------------------------------		  
% margins		  
\RequirePackage[left=2cm,%
                right=2cm,%
		top=2.25cm,%
		bottom=2.25cm,%
		headheight=11pt,%
		letterpaper]{geometry}%
\RequirePackage[labelfont={bf,sf},%
                labelsep=period,%
                justification=raggedright]{caption}
% ---------------------------------------------------------------------
\RequirePackage{fancyhdr}  % Needed to define custom headers/footers
\RequirePackage{lastpage}  % Number of pages in the document
\pagestyle{fancy}          % Enables the custom headers/footers

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STEFAN
\RequirePackage[sc]{mathpazo}    % lijepe jednadžba
%\RequirePackage[hang]{footmisc}
\RequirePackage{footmisc}
\renewcommand*{\footnotelayout}{\color{simgrey}\footnotesize\sffamily}

\renewcommand*\sfdefault{ppl}
\definecolor{simblue}{rgb}{0.0,0.5686,0.9294}
\definecolor{simgrey}{rgb}{0.28,0.28,0.28}
\definecolor{simlightgrey}{rgb}{0.5,0.5,0.5}
\newcommand*{\colorrule}[2][1ex]{{\color{simblue}{\rule{#2}{#1}}}}
\newcommand*{\colorsep}{{\color{simblue}{\rule{5pt}{5pt}}}}
\newcommand*{\whitesep}{{\color{white}{\rule{5pt}{5pt}}}}

\renewcommand{\Huge}{\fontsize{14}{0}}
\renewcommand{\huge}{\fontsize{13}{0}}
\renewcommand{\LARGE}{\fontsize{12}{0}}
\renewcommand{\Large}{\fontsize{11}{0}}
\renewcommand{\large}{\fontsize{10}{0}}
\renewcommand{\normalsize}{\fontsize{9}{10}}
\renewcommand{\small}{\fontsize{8}{0}}
\renewcommand{\footnotesize}{\fontsize{7}{0}}
\renewcommand{\scriptsize}{\fontsize{6}{0}}
\renewcommand{\tiny}{\fontsize{5}{0}}

\PassOptionsToPackage{pdftex,hyperfootnotes=false,pdfpagelabels}{hyperref}
\usepackage{hyperref}  % backref linktocpage pagebackref
\pdfcompresslevel=9
\pdfadjustspacing=1

\hypersetup{
% Uncomment the line below to remove all links (to references, figures, tables, etc)
%draft, 
colorlinks=true,
linktocpage=true,
pdfstartpage=3,
pdfstartview=FitV,
% Uncomment the line below if you want to have black links (e.g. for printing black and white)
%colorlinks=false, linktocpage=false, pdfborder={0 0 0}, pdfstartpage=3, pdfstartview=FitV, 
%breaklinks=true, pdfpagemode=UseNone, pageanchor=true, pdfpagemode=UseOutlines,
%plainpages=false, bookmarksnumbered, bookmarksopen=true, bookmarksopenlevel=1,
%hypertexnames=true, pdfhighlight=/O, 
urlcolor=simblue,
linkcolor=simblue,
citecolor=simblue,
%------------------------------------------------
% PDF file meta-information
%pdftitle={\myTitle},
%pdfauthor={\textcopyright\ \myName, \myUni, \myFaculty},
%pdfsubject={},
%pdfkeywords={},
%pdfcreator={pdfLaTeX},
%pdfproducer={LaTeX with hyperref and classicthesis}
%------------------------------------------------
}   

\usepackage{setspace}
\setstretch{1.2}

%\DeclareCaptionFont{simcaptionblue}{\color{simblue}}
\DeclareCaptionFont{simcaptionfont}{\small\color{simgrey}}
\DeclareCaptionLabelSeparator{simcaptionseparator}{~\colorsep~}

\captionsetup[figure]{
labelfont={simcaptionfont,bf},
textfont={simcaptionfont,normalfont},
singlelinecheck=off,
justification=raggedright,
labelsep=simcaptionseparator
}
\captionsetup[table]{
labelfont={simcaptionfont,bf},
textfont={simcaptionfont,normalfont},
singlelinecheck=off,
justification=raggedright,
labelsep=simcaptionseparator
}

\let\reftagform@=\tagform@
\def\tagform@#1{\maketag@@@{\textcolor{simgrey}{(\ignorespaces #1\unskip\@@italiccorr)}}}
\renewcommand{\eqref}[1]{\textup{\reftagform@{\ref{#1}}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Headers
\usepackage{tikz}
%\usepackage{kpfonts}
\lhead{}%
\chead{}%
%\rhead{\small\sffamily\bfseries\@PaperTitle\  --- \thepage/\pageref*{LastPage}}
\rhead{
\begin{tikzpicture}[remember picture,overlay]
    \node[yshift=-2cm] at (current page.north west)
      {
      \begin{tikzpicture}[remember picture, overlay]
        \draw[draw=none, fill=simlightgrey] (0,0cm) rectangle
          (\paperwidth,2cm);
        \node[anchor=east,xshift=.9\paperwidth,yshift=.5cm,rectangle,
              %rounded corners=2pt,inner sep=11pt,
              %fill=simgrey
              ]
              {\color{white}\normalsize\sffamily\@PaperTitle\ ~~\whitesep};
        \node[anchor=east,xshift=.95\paperwidth,yshift=.5cm,rectangle,
              %rounded corners=2pt,inner sep=11pt,
              %fill=simgrey
              ]
              {\color{white}\normalsize\sffamily\thepage/\pageref*{LastPage}};
       \end{tikzpicture}
      };
   \end{tikzpicture}
}   
% Footers
\lfoot{}%
\cfoot{\url{sim.riteh.hr}}%
\rfoot{}%
\renewcommand{\headrulewidth}{0pt}% % No header rule
\renewcommand{\footrulewidth}{0pt}% % No footer rule
% ---------------------------------------------------------------------
% section/subsection/paragraph set-up
\RequirePackage[explicit]{titlesec}
\titleformat{\section}
  {\color{simblue}\large\sffamily\bfseries}
  {}
  {0em}
  {\color{simblue}~\arabic{section}. #1}
  []
\titleformat{name=\section,numberless}
  {\color{simblue}\large\sffamily\bfseries}
  {}
  {0em}
  {\color{simblue}~#1}
  []  
\titleformat{\subsection}
  {\color{simgrey}\normalsize\sffamily\bfseries}
  {\thesubsection}
  {0.5em}
  {#1}
  []
\titleformat{\subsubsection}
  {\sffamily\color{simgrey}\normalsize\bfseries\itshape}
  {\thesubsubsection}
  {0.5em}
  {#1}
  []    
\titleformat{\paragraph}[runin]
  {\sffamily\small\mdseries}
  {}
  {0em}
  {#1} 
\titlespacing*{\section}{0pc}{3ex \@plus4pt \@minus3pt}{5pt}
\titlespacing*{\subsection}{0pc}{2.5ex \@plus3pt \@minus2pt}{2pt}
\titlespacing*{\subsubsection}{0pc}{2ex \@plus2.5pt \@minus1.5pt}{2pt}
\titlespacing*{\paragraph}{0pc}{1.5ex \@plus2pt \@minus1pt}{10pt}
% ---------------------------------------------------------------------
% tableofcontents set-up
\usepackage{titletoc}
\contentsmargin{0cm}
\titlecontents{section}[\tocsep]
  {\addvspace{4pt}\small\bfseries\sffamily}
  {\contentslabel[\thecontentslabel]{\tocsep}}
  {}
  {\hfill\thecontentspage}
  []
\titlecontents{subsection}[\tocsep]
  {\addvspace{2pt}\small\sffamily}
  {\contentslabel[\thecontentslabel]{\tocsep}}
  {}
  {\ \titlerule*[.5pc]{.}\ \thecontentspage}
  []
\titlecontents*{subsubsection}[\tocsep]
  {\footnotesize\sffamily}
  {}
  {}
  {}
  [\ \colorsep\ ]  
% ---------------------------------------------------------------------  
% Get the multiple author set
\newcount\@authcnt
\newcount\@tmpcnt\@tmpcnt\z@

\def\@affiliation{%
  \ifnum\@tmpcnt<\@authcnt
   \global\advance\@tmpcnt1
    \raggedright \csname @auth\romannumeral\the\@tmpcnt\endcsname\hfill\\%
   \let\next\@affiliation \vskip1pt
  \else
   \let\next\relax
  \fi
\next}
	 
\newcommand{\affiliation}[1]{%
    \global\advance\@authcnt1
    \expandafter\gdef\csname @auth\romannumeral\the\@authcnt\endcsname
    {#1}}
% ---------------------------------------------------------------------
\RequirePackage{enumitem}
%\setlist{nolistsep} % Uncomment to remove spacing between items in lists (enumerate, itemize)
% ---------------------------------------------------------------------
% Remove brackets from numbering in List of References
\renewcommand{\@biblabel}[1]{\bfseries\color{simgrey}[#1]}
%\setlength{\bibitemsep}{0cm}
% ---------------------------------------------------------------------
\newcommand{\PaperTitle}[1]{\def\@PaperTitle{#1}}
\newcommand{\Archive}[1]{\def\@Archive{#1}}
\newcommand{\Authors}[1]{\def\@Authors{#1}}
\newcommand{\JournalInfo}[1]{\def\@JournalInfo{#1}}
\newcommand{\Abstract}[1]{\def\@Abstract{#1}}
\newcommand{\Keywords}[1]{\def\@Keywords{#1}}
% ---------------------------------------------------------------------
\renewcommand{\@maketitle}{%
\twocolumn[{%
\thispagestyle{empty}%
\vskip-36pt%
{\raggedleft\normalsize\sffamily\@JournalInfo\\\@Archive\par}%
\vskip20pt%
{\raggedright\color{simblue}\sffamily\fontsize{16}{25}\selectfont \@PaperTitle\par}%
\vskip10pt
{\raggedright\color{simgrey}\sffamily\fontsize{11}{16}\selectfont  \@Authors\par}
\vskip18pt%
%\fcolorbox{simblue}{white}{%
  \parbox{\textwidth-2\fboxsep-2\fboxrule}{
    \centering%
    %\colorbox{color2!10}{%
      \parbox{\textwidth-3.5\fboxsep-3.5\fboxrule}{%
	\ifx\@Keywords\@empty
	  \color{simgrey}\sffamily\small\textbf{\abstractname}\\\@Abstract
	\else
	  \color{simgrey}\sffamily\small\textbf{\abstractname}\\\@Abstract\\[5pt]%
	  \textbf{\keywordname}\\\@Keywords%
	\fi
      }%
    %}%
    \vskip5pt%
    \begingroup%
    \raggedright\sffamily\small%
    \color{simgrey}\footnotesize\@affiliation\par%
    \endgroup%%
  }%
%}%
\vskip5pt%
\colorrule[5pt]{\textwidth}
\vskip5pt%
}]%
}%
% ---------------------------------------------------------------------
\let\oldbibliography\thebibliography
\renewcommand{\thebibliography}[1]{%
\addcontentsline{toc}{section}{\hspace*{-\tocsep}\refname}%
\color{simgrey}\oldbibliography{#1}%
\setlength\itemsep{0pt}%
}